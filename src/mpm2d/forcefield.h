#ifndef FORCEFIELD_H
#define FORCEFIELD_H

#include <Eigen/Jacobi>
#include <Eigen/Dense>

namespace mpm {

class ForceField {

    using Mat2d = Eigen::Matrix<double, 2, 2>;

public:

    enum class ForceModel { corotation, stvk, neohookean };

    ForceField(ForceModel m = ForceModel::stvk): model(m)    {}

    inline void computeForce(const Mat2d& F, double mu, double lambda, double vol, Mat2d& sigma) {
        switch (model) {
            case ForceModel::corotation : {
                Eigen::Affine2d af(F);
                Mat2d R = af.rotation();
                Mat2d S = R.transpose()*F;
                double J = S(0,0)*S(1,1);
                sigma = vol*(mu*(F - R)*F + lambda*(J - 1)*J*Eigen::Matrix2d::Identity());
                break;
            }
            case ForceModel::stvk : {
                Mat2d b = F*F.transpose();
                sigma = vol*(mu*b*(b - Eigen::Matrix2d::Identity()) + (0.5*lambda)*(b.trace() - 2)*F.transpose());
                break;
            }
            case ForceModel::neohookean : {
                double J = F.determinant();
                sigma = vol*(mu*(F*F.transpose() - Eigen::Matrix2d::Identity()) + (lambda*log(J))*Eigen::Matrix2d::Identity());
                break;
            }
            default : {
                printf("Force model not supported! \n");
                break;
            }
        }
    }

    inline void computeForceDiff(const Mat2d& dF, const Mat2d& F, double mu, double lambda, Mat2d& dsigma) {

    }

private:
    ForceModel model;
};

}

#endif // FORCEFIELD_H
