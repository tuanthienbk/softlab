#ifndef MESHLOADER2D_H
#define MESHLOADER2D_H

using Vec2d = Eigen::Vector2d;
using Vec3i = Eigen::Vector3i;

using VecCoord2d = std::vector<Vec2d>;
using VecCoord3i = std::vector<Vec3i>;

namespace mpm {

struct MeshLoader {

    static void loadOFF(const char* filename, VecCoord2d& v,  VecCoord3i& f, unsigned int& nV, unsigned int& nF) {

        FILE *offfile = fopen(filename,"r");
        if (offfile == NULL)
        {
            printf("File not found!\n");
            return;
        }
        char header[5];
        fscanf(offfile,"%s\n", header);
        if (strcmp(header,"OFF"))
        {
            printf("This is not OFF file\n");
            return;
        }
        fscanf(offfile,"%d %d%*[^\n]", &nV, &nF);
        v.resize(nV);
        f.resize(nF);

        float x, y, z;
        for(int i = 0; i < nV; i++)
        {
            fscanf(offfile,"%f %f %f%*[^\n]",&x, &y, &z);
            v[i] << double(x), double(y);

        }

        for(int i = 0; i < nF; i++)
        {
            int np;
            fscanf(offfile,"%d %d %d %d%*[^\n]",&np, &f[i][0], &f[i][1], &f[i][2]);

        }
        fclose(offfile);

    }
};



}

#endif // MESHLOADER2D_H
