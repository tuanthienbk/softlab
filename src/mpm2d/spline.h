#ifndef SPLINE_H
#define SPLINE_H

namespace mpm {
    void linearSpline(const double& u, const double& h, double* B, double* Bx);
    void cubicSpline(const double& u,const double& h, double* B, double* Bx);
}

#endif // SPLINE_H
