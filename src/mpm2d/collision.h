#ifndef COLLISION_H
#define COLLISION_H

#include "ptobject.h"

namespace mpm {
void doCollision(ptObject* obj1, ptObject* obj2) {
    Grid* g1 = obj1->grid;
    Grid* g2 = obj2->grid;
    for(auto& it: g1->internal) {
        std::pair<int,int> pr = it.first;
        GridInfo *node2 = g2->getNode(pr);
        if (node2) {
            GridInfo *node1 = &it.second;
            Vec2d n1 = node1->massdiff/node1->massdiff.norm();
            Vec2d n2 = node2->massdiff/node2->massdiff.norm();
            Vec2d n12 = (n1-n2)/(n1-n2).norm();
            if ((node1->vf - node2->vf).dot(n12) > 0) {
                Vec2d v_com = (node1->vf*node1->mass + node2->vf*node2->mass)/(node1->mass + node2->mass);
                node1->vf = node1->vf - ((node1->vf - v_com).dot(n12))*n12;
                node2->vf = node2->vf - ((node2->vf - v_com).dot(n12))*n12;
            }
        }
    }
}
}

#endif // COLLISION_H
