#include "ptobject.h"
#include "spline.h"

namespace mpm {

void ptObject::stepNode(double dt)
{
    grid->reset();
    for (int k = 0; k < nP; ++k) {
        Mat2d sigma;
        ff->computeForce(F[k], mu, lambda, vol[k], sigma);

        Vec2d& p = pos[k];
        int i, j;
        double u, v;
        grid->getCoord(p.x(),p.y(),i,j,u,v);
        double Bx[4], dBx[4], By[4], dBy[4];
        cubicSpline(u,grid->hx,Bx,dBx);
        cubicSpline(v,grid->hy,By,dBy);
        for (int i1 = 0; i1 < 4; ++i1) {
            for (int i2 = 0; i2 < 4; ++i2) {
                std::pair<int,int> idx = std::make_pair(i-1+i1, j-1+i2);
                GridInfo* node = grid->getNode(idx);
                if (node) {
                    node->mass += Bx[i1]*By[i2]*mass[k];
                    node->massdiff(0) += dBx[i1]*By[i2]*mass[k];
                    node->massdiff(1) += Bx[i1]*dBy[i2]*mass[k];
                    // this is actually momentum, later we divide by the mass to obtain velocities
                    node->vp += Bx[i1]*By[i2]*mass[k]*vel[k];
                    node->force(0) += - (sigma(0,0)*dBx[i1]*By[i2] + sigma(0,1)*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*mass[k]*gravity[0];
                    node->force(1) += - (sigma(1,0)*dBx[i1]*By[i2] + sigma(1,1)*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*mass[k]*gravity[1];
                }
                else {
                    node = new GridInfo;
                    node->mass = Bx[i1]*By[i2]*mass[k];
                    node->massdiff(0) = dBx[i1]*By[i2]*mass[k];
                    node->massdiff(1) = Bx[i1]*dBy[i2]*mass[k];
                    // this is actually momentum, later we divide by the mass to obtain velocities
                    node->vp = Bx[i1]*By[i2]*mass[k]*vel[k];
                    node->force(0) = - (sigma(0,0)*dBx[i1]*By[i2] + sigma(0,1)*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*mass[k]*gravity[0];
                    node->force(1) = - (sigma(1,0)*dBx[i1]*By[i2] + sigma(1,1)*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*mass[k]*gravity[1];
                    grid->setNode(idx, *node);
                }
            }
        }
    }
    grid->updateVelocitiesExplicit(dt);
}

void ptObject::stepParticle(double dt)
{
    VecCoord2d dv(nP);
    VecMat2d L(nP);
    for (int k = 0; k < nP; ++k) {
        L[k] = Eigen::Matrix2d::Zero();
        dv[k] << 0, 0;
        Vec2d& p = pos[k];
        int i, j;
        double u, v;
        grid->getCoord(p.x(),p.y(),i,j,u,v);
        double Bx[4], dBx[4], By[4], dBy[4];
        cubicSpline(u,grid->hx,Bx,dBx);
        cubicSpline(v,grid->hy,By,dBy);
        for (int i1 = 0; i1 < 4; ++i1) {
            for (int i2 = 0; i2 < 4; ++i2) {
                std::pair<int,int> idx = std::make_pair(i-1+i1, j-1+i2);
                GridInfo* node = grid->getNode(idx);
                if (node->mass > 1e-12) {
                    dv[k] +=  (node->vf - alpha*node->vp)*Bx[i1]*By[i2];
                    L[k].col(0) += (dBx[i1]*By[i2])*node->vf;
                    L[k].col(1) += (Bx[i1]*dBy[i2])*node->vf;
                }

            }
        }
    }

    for (int k = 0; k < nP; ++k) {
        vel[k] = alpha*vel[k] + dv[k];
        pos[k] += dt*vel[k];
        // collision
        if (pos[k](1) <= -2) {
            vel[k] *= -1;
        }
        F[k] = (Eigen::Matrix2d::Identity() + dt*L[k])*F[k];
    }


}


}
