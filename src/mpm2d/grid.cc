#include "grid.h"
#include <cmath>

namespace mpm {

void Grid::getCoord(const double &x, const double &y, int &i, int &j, double &u, double &v)
{
    double xx = x/hx;
    double yy = y/hy;
    i = int(floor(xx));
    j = int(floor(yy));
    u = xx - i;
    v = yy - j;
}

void Grid::updateVelocitiesExplicit(const double &dt)
{
    for(auto& grid: internal) {
        GridInfo& node = grid.second;
        node.vp = node.vp/node.mass;
        node.vf = node.vp + (dt/node.mass)*node.force;
        //hack
        if (grid.first.second < -2/hy) {
            node.vf = 0*node.vf;
        }
    }
}

}
