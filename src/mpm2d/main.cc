#include "ptobject.h"
#include "collision.h"

#include <vdb.h>

using namespace mpm;

int main(int argv, char **argc) {
    TriangleMesh *mesh = new TriangleMesh("circ3.off");
    Vec2d trans(0,-1);
    mesh->translate(trans);

    Grid* grid1 = new Grid(0.2, 0.2);

    ForceField *ff = new ForceField(ForceField::ForceModel::neohookean);

    Vec2d gravity(0.0,-9.8);
    ptObject *obj1 = new ptObject(grid1, mesh, ff, 100., gravity, 1.5e4, 0.2, 0.95);
    obj1->setColor(1.0,0.0,0.0);

    trans << 0.2,2.2 ;
    mesh->translate(trans);

    Grid* grid2 = new Grid(0.2, 0.2);
    ptObject *obj2 = new ptObject(grid2, mesh, ff, 100., gravity, 1.5e5, 0.2, 0.95);
    obj2->setColor(0.0,1.0,0.0);

    std::vector<ptObject*> objList;
    objList.push_back(obj1);
    objList.push_back(obj2);

    std::vector<std::pair<ptObject*, ptObject*> > collision_pairs;
    collision_pairs.push_back( std::make_pair(obj1, obj2));

    double dt = 1e-3;
    while (1) {
        for(auto o: objList) {
            o->stepNode(dt);
        }

        for(auto p: collision_pairs) {
            doCollision(p.first, p.second);
        }

        for(auto o: objList) {
            o->stepParticle(dt);
        }

        vdb_begin();
        vdb_frame();
        for(auto o: objList) {
            float* color = o->getColor();
            vdb_color(color[0], color[1], color[2]);
            for (int k = 0; k < o->pos.size(); ++k) {
                vdb_point(o->pos[k].x(), o->pos[k].y(), 0.0);
            }
        }
        vdb_end();
    }

    return 0;
}


