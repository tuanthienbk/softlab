#ifndef MESH_H
#define MESH_H

#include <stdio.h>
#include "meshLoader.h"

namespace mpm {

class TriangleMesh
{

public:
    unsigned int nV, nF;
    VecCoord2d vertices; // vertex coordinates
    VecCoord3i faces; // face list

public:
    TriangleMesh(const char* filename) {
        std::string fn(filename);
        std::string fext = fn.substr(fn.find_last_of(".") + 1);
        if ( fext == "off") {
            MeshLoader::loadOFF(filename, vertices, faces, nV, nF);
        } else {
            printf("Only OFF format are supported \n");
        }
    }

    inline double getArea(const unsigned int& fid) const {
        Vec2d v10 = vertices[faces[fid][1]] - vertices[faces[fid][0]];
        Vec2d v20 = vertices[faces[fid][2]] - vertices[faces[fid][0]];
        return 0.5*(v10.x()*v20.y() - v10.y()*v20.x());
    }
    inline Vec2d getCentroid(const unsigned int& fid) const {
        return  (vertices[faces[fid][0]] + vertices[faces[fid][1]] + vertices[faces[fid][2]])/3;
    }

    inline void translate(Vec2d& v)
    {
        for (int k = 0; k < nV; ++k) {
           vertices[k] += v;
        }
    }

};

}

#endif // MESH_H
