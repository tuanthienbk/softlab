#ifndef PTOBJECT_H
#define PTOBJECT_H

#include "grid.h"
#include "mesh.h"
#include "forcefield.h"

namespace mpm {
/**
 * @brief The particle object
 */
class ptObject {
    using Mat2d = Eigen::Matrix<double, 2, 2>;
    using VecMat2d = std::vector<Mat2d>;
public:
    Grid* grid;
    VecCoord2d pos;

private:
    ForceField* ff;
    Vec2d gravity;
    double mu, lambda, alpha;
    unsigned int nP;
    std::vector<double> mass;
    std::vector<double> vol;
    VecCoord2d vel;   
    VecMat2d F;
    float color[3];

public:

    ptObject(Grid* g, const TriangleMesh* m, ForceField* ff, double rho, Vec2d gravity, double E, double nu, double alpha)
        : grid (g),
          ff(ff),
          gravity(gravity),
          alpha(alpha)
    {
        mu = E/(2*(1+nu));
        lambda = E*nu/((1-2*nu)*(1+nu));
        nP = m->nF;
        mass.resize(nP);
        vol.resize(nP);
        vel.resize(nP);
        pos.resize(nP);
        F.resize(nP);
        for (int i = 0; i < nP; ++i) {
            vol[i] = m->getArea(i);
            mass[i] = vol[i]*rho;
            pos[i] = m->getCentroid(i);
            F[i] = Eigen::Matrix2d::Identity();
        }
    }

    void stepNode(double dt);

    void stepParticle(double dt);

    inline void setColor(float r, float g, float b) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
    inline float* getColor() {
        return color;
    }
};

}

#endif // PTOBJECT_H
