#include "spline.h"

namespace mpm {
    void linearSpline(const double& u,const double& h, double* B, double* Bx) {
        B[0] = 1-u;
        B[1] = u;
        Bx[0] = -1/h;
        Bx[1] = 1/h;
    }

    void cubicSpline(const double& u,const double& h, double* B, double* Bx) {
        B[0] = (1/6.0)*(1-u)*(1-u)*(1-u);
        B[1] = (1/6.0)*(4-6*u*u+3*u*u*u);
        B[2] = (1/6.0)*(1+3*u+3*u*u-3*u*u*u);
        B[3] = (1/6.0)*(u*u*u);
        Bx[0] = -(1/(2.0*h))*(1-u)*(1-u);
        Bx[1] = (1/(6.0*h))*(-12*u+9*u*u);
        Bx[2] = (1/(6.0*h))*(3+6*u-9*u*u);
        Bx[3] = (1/(2.0*h))*(u*u);
    }
}

