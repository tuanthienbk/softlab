#ifndef GRID_H
#define GRID_H

#include <map>
#include <Eigen/Dense>
#include <cstddef>

namespace mpm {

using Vec2d = Eigen::Matrix<double, 2, 1>;

struct GridInfo
{
    double mass;
    Vec2d massdiff;
    Vec2d vp;
    Vec2d vf;
    Vec2d force;
    Vec2d du;
    Vec2d df;
};

struct Grid {
    Grid(const double& h1, const double& h2): hx(h1), hy(h2) {}

    void getCoord(const double& x, const double& y, int& i, int& j , double& u, double& v);

    void updateVelocitiesExplicit(const double& dt);

    inline void reset() { internal.clear(); }

    GridInfo* getNode(std::pair<int,int>& p) {
        auto it =  internal.find(p);
        if ( it == internal.end())
            return NULL;
        else
            return &it->second;
    }

    void setNode(const std::pair<int,int>& p, const GridInfo& node) {
        internal[p] = node;
    }   

public:
    double hx, hy;
    std::map<std::pair<int,int>,GridInfo> internal;
};



}
#endif // GRID_H
