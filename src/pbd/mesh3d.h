#ifndef MESH3D_H
#define MESH3D_H

#include <stdio.h>
#include "meshLoader.h"

namespace pbd {

struct Mesh {

    enum class Type {
        Triangular,
        Quadrangular,
        Volumetric,
        Tetrahedral,
        Hexahedral
    };

    Type type;
protected:
    unsigned int nV, nEle;
    VecCoord3d vertices; // vertex coordinates
public:
    inline void translate(Vec3d& v)
    {
        for (int k = 0; k < nV; ++k) {
           vertices[k] += v;
        }
    }

    inline void scale(Vec3d& v)
    {
        for (int k = 0; k < nV; ++k) {
           vertices[k][0] *= v[0];
           vertices[k][1] *= v[1];
           vertices[k][2] *= v[2];
        }
    }

    inline VecCoord3d& getVerticesPositions() { return vertices; }

    inline Vec3d& getVerticesPosition(const unsigned int& idx) { return vertices.at(idx); }

    inline const int getNumVertices() const { return nV; }

    inline void setNumVertices(const unsigned int& n) { nV = n; }

    inline const int getNumElements() const { return nEle; }

    inline void setNumElements(const unsigned int& n) { nEle = n; }

    inline Type getType() const { return type; }

//    inline void computeAABB(double& min_x, double& max_x, double& min_y, double& max_y, double& min_z, double& max_z)
//    {
//        min_x = 1e6;
//        max_x = -1e6;
//        min_y = 1e6;
//        max_y = -1e6;
//        min_z = 1e6;
//        max_z = -1e6;
//        for (int i = 0; i < nV; ++i) {
//           Vec3d& p = vertices[i];
//           if (p[0] < min_x) min_x = p[0];
//           if (p[0] > max_x) max_x = p[0];
//           if (p[1] < min_y) min_y = p[1];
//           if (p[1] > max_y) max_y = p[1];
//           if (p[2] < min_z) min_z = p[2];
//           if (p[2] > max_z) max_z = p[2];
//        }
//    }
};

class TriangleMesh : public Mesh
{

private:

    VecCoord3i faces; // face list

public:
    TriangleMesh()
    {
        type = Mesh::Type::Triangular;
    }

    TriangleMesh(const char* filename)
    {

        type = Mesh::Type::Triangular;

        std::string fn(filename);
        std::string fext = fn.substr(fn.find_last_of(".") + 1);
        if ( fext == "off") {
            MeshLoader::loadOFF(filename, vertices, faces, nV, nEle);
        } else {
            printf("Only OFF format are supported \n");
        }
    }

    inline void asSquareCloth(int w, int h, int nRows, int nCols) {
        MeshLoader::createSquareCloth(w,h,nRows,nCols,vertices,faces,nV,nEle);
    }

    inline double getArea(const unsigned int& fid) const {
        Vec3d v10 = vertices[faces[fid][1]] - vertices[faces[fid][0]];
        Vec3d v20 = vertices[faces[fid][2]] - vertices[faces[fid][0]];
        return 0.5*v10.cross(v20).norm();
    }

    inline Vec3d getCentroid(const unsigned int& fid) const {
        return  (vertices[faces[fid][0]] + vertices[faces[fid][1]] + vertices[faces[fid][2]])/3;
    }

    inline Vec3i& getElement(const unsigned int& idx) { return faces.at(idx); }

    inline VecCoord3i& getElements() { return faces; }

    inline void flipDirection()
    {
        for (int k = 0; k < faces.size(); ++k) {
           Vec3i& e = faces[k];
           std::swap(e[1],e[2]);
        }
    }
};

struct VolumetricMesh : public Mesh
{

public:

    VolumetricMesh()
    {
        type = Mesh::Type::Volumetric;
    }



};

class TetMesh : public VolumetricMesh
{

private:
    VecCoord4i elements; // face list

public:

    TetMesh()
    {
        type = Mesh::Type::Tetrahedral;
    }

    TetMesh(const char* filename)
    {
        type = Mesh::Type::Tetrahedral;

        std::string fn(filename);
        std::string fext = fn.substr(fn.find_last_of(".") + 1);
        if ( fext == "veg") {
            MeshLoader::loadVEG(filename, vertices, elements, nV, nEle);
        } else {
            printf("Only VEG format are supported \n");
        }
    }

    inline void asBeam(int width, int height, int depth) {
        MeshLoader::createBarMesh(width,height,depth,vertices,elements,nV,nEle);
    }

    inline Vec4i& getElement(const unsigned int& idx) { return elements.at(idx); }

    inline VecCoord4i& getElements() { return elements; }
};

}

#endif // MESH3D_H
