#ifndef COLLISION_H
#define COLLISION_H

#include "pbdCollisionConstraint.h"
#include "pbdCollisionModel.h"

namespace pbd
{

class Collision
{
private:
    std::vector<CollisionConstraint*>    m_collisionConstraints;

public:
    Collision() {}
    inline void resetConstraints()
    {
        m_collisionConstraints.clear();
    }

    bool doBroadPhase(CollisionModel* first, CollisionModel* second);
    void doNarrowPhase(CollisionModel* first, CollisionModel* second);
    void doCollision(const int& maxIter);
};

}
#endif // COLLISION_H
