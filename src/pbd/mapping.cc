#include "bits/stdc++.h"

#include "mapping.h"
#include "pbdCollisionModel.h"
#include "pbdModel.h"

namespace pbd {

struct FaceComp {
    bool operator()(const Vec3i& f1, const Vec3i& f2) {
        int a[3];
        int b[3];
        a[0] = f1[0];
        a[1] = f1[1];
        a[2] = f1[2];
        b[0] = f2[0];
        b[1] = f2[1];
        b[2] = f2[2];
        std::sort(a,a+3);
        std::sort(b,b+3);
        if (a[0] < b[0]) return true;
        else if (a[0] > b[0]) return false;
        else {
            if (a[1] < b[1]) return true;
            else if (a[1] > b[1]) return false;
            else {
                return a[2] < b[2];
            }
        }
        return true;
    }
};

void IdentityMapping::computeMap(TetMesh* vmesh, TriangleMesh* tmesh)
{
    std::map<Vec3i, int, FaceComp> visited;
    for (int i = 0; i < vmesh->getNumElements(); ++i) {
        Vec4i e = vmesh->getElement(i);
        Vec3i f1, f2, f3, f4;
        f1 << e[0],e[2],e[1];
        f2 << e[1],e[3],e[2];
        f3 << e[2],e[0],e[3];
        f4 << e[3],e[0],e[1];
        visited[f1]++;
        visited[f2]++;
        visited[f3]++;
        visited[f4]++;
    }
    VecCoord3i& faces = tmesh->getElements();
    for(auto &x: visited) {
        if (x.second == 1) {
            faces.push_back(x.first);
        }
    }

    for (int i = 0; i < faces.size(); ++i) {
       Vec3i& f = faces[i];
       if (vmap.find(f[0]) == vmap.end()) vmap[f[0]] = vmap.size();
       if (vmap.find(f[1]) == vmap.end()) vmap[f[1]] = vmap.size();
       if (vmap.find(f[2]) == vmap.end()) vmap[f[2]] = vmap.size();
    }
    VecCoord3d& v2 = tmesh->getVerticesPositions();
    VecCoord3d& v1 = vmesh->getVerticesPositions();
    v2.resize(vmap.size());

    for(auto& x: vmap) {
        v2[x.second] = v1[x.first];
    }
    for (int i = 0; i < faces.size(); ++i) {
        Vec3i& f = faces[i];
        f[0] = vmap[f[0]];
        f[1] = vmap[f[1]];
        f[2] = vmap[f[2]];
    }

    tmesh->setNumVertices(v2.size());
    tmesh->setNumElements(faces.size());
}

void IdentityMapping::applyMapping(PositionBasedModel *model, CollisionModel *cmodel)
{
    for(auto& x: vmap) {
        cmodel->getState()->getVertexPosition(x.second) = model->getState()->getVertexPosition(x.first);
    }
}

void IdentityMapping::applyMapping(CollisionModel *cmodel, PositionBasedModel *model)
{
    for(auto& x: vmap) {
        model->getState()->getVertexPosition(x.first) = cmodel->getState()->getVertexPosition(x.second);
    }
}

}
