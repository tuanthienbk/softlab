#ifndef MESHLOADER_H
#define MESHLOADER_H

#include <Eigen/Dense>

using Vec3d = Eigen::Vector3d;
using Vec3i = Eigen::Vector3i;
using Vec4i = Eigen::Vector4i;

using VecCoord3d = std::vector<Vec3d>;
using VecCoord3i = std::vector<Vec3i>;
using VecCoord4i = std::vector<Vec4i>;

namespace pbd {

struct MeshLoader {

    static void loadOFF(const char* filename, VecCoord3d& v,  VecCoord3i& f, unsigned int& nV, unsigned int& nF) {

        FILE *offfile = fopen(filename,"r");
        if (offfile == NULL)
        {
            printf("File not found!\n");
            return;
        }
        char header[5];
        fscanf(offfile,"%s\n", header);
        if (strcmp(header,"OFF"))
        {
            printf("This is not OFF file\n");
            return;
        }
        fscanf(offfile,"%d %d%*[^\n]", &nV, &nF);
        v.resize(nV);
        f.resize(nF);

        float x, y, z;
        for(int i = 0; i < nV; i++)
        {
            fscanf(offfile,"%f %f %f%*[^\n]",&x, &y, &z);
            v[i] << double(x), double(y), double(z);

        }

        for(int i = 0; i < nF; i++)
        {
            int np;
            fscanf(offfile,"%d %d %d %d%*[^\n]",&np, &f[i][0], &f[i][1], &f[i][2]);

        }
        fclose(offfile);

    }

    static void createSquareCloth(int width, int height, int nRows, int nCols, VecCoord3d& v,  VecCoord3i& f, unsigned int& nV, unsigned int& nF) {
        nV = nRows*nCols;
        v.resize(nV);
        const double dy = width / (double)(nCols - 1);
        const double dx = height / (double)(nRows - 1);
        for (int i = 0; i < nRows; i++)
        {
            for (int j = 0; j < nCols; j++)
            {
                const double y = (double)dy*j;
                const double x = (double)dx*i;
                v[i*nCols + j] = Vec3d(x, 1.0, y);

            }
        }

        for (int i = 0; i < nRows - 1; i++)
        {
            for (int j = 0; j < nCols - 1; j++)
            {
                f.push_back(Vec3i(i*nCols + j, (i + 1)*nCols + j , i*nCols + j + 1));

                f.push_back(Vec3i((i + 1)*nCols + j + 1, i*nCols + j + 1, (i + 1)*nCols + j));
            }
        }
        nF = f.size();
    }

    static void loadVEG(const char* filename, VecCoord3d& v,  VecCoord4i& f, unsigned int& nV, unsigned int& nF) {

        FILE *vegfile = fopen(filename,"r");
        if (vegfile == NULL)
        {
            printf("File not found!\n");
            return;
        }

        char header[100];

        while (1) {

            fscanf(vegfile,"%s%*[^\n]", header);

            if (feof(vegfile)) break;

            if (strcmp(header,"*VERTICES") == 0)
            {
                int dim;
                fscanf(vegfile,"%d %d%*[^\n]", &nV, &dim);
                v.resize(nV);

                int num;
                float x, y, z;
                for(int i = 0; i < nV; i++)
                {
                    fscanf(vegfile,"%d %f %f %f%*[^\n]",&num, &x, &y, &z);
                    v[i] << double(x), double(y), double(z);
                }
            }
            else if (strcmp(header,"*ELEMENTS") == 0) {
                char type[10];
                fscanf(vegfile,"%s\n", type);
                int dim;
                fscanf(vegfile,"%d %d%*[^\n]", &nF, &dim);
                f.resize(nF);

                for(int i = 0; i < nF; i++)
                {
                    int np;
                    fscanf(vegfile,"%d %d %d %d %d%*[^\n]",&np, &f[i][0], &f[i][1], &f[i][2], &f[i][3]);
                    f[i] -= Vec4i(1,1,1,1);
                }

            }

        }


        fclose(vegfile);

    }
    static void createBarMesh(int w, int h, int d, VecCoord3d& v,  VecCoord4i& f, unsigned int& nV, unsigned int& nF) {
        nV = w*h*d;
        v.resize(nV);
        for (unsigned int i = 0; i < w; i++)
        {
            for (unsigned int j = 0; j < h; j++)
            {
                for (unsigned int k = 0; k < d; k++)
                {
                    v[i*h*d + j*d + k] = 0.3*Vec3d(double(i), double(j), double(k));
                }
            }
        }
        for (unsigned int i = 0; i < w - 1; i++)
        {
            for (unsigned int j = 0; j < h - 1; j++)
            {
                for (unsigned int k = 0; k < d - 1; k++)
                {
                    // For each block, the 8 corners are numerated as:
                    //     4*-----*7
                    //     /|    /|
                    //    / |   / |
                    //  5*-----*6 |
                    //   | 0*--|--*3
                    //   | /   | /
                    //   |/    |/
                    //  1*-----*2
                    unsigned int p0 = i*h*d + j*d + k;
                    unsigned int p1 = p0 + 1;
                    unsigned int p3 = (i + 1)*h*d + j*d + k;
                    unsigned int p2 = p3 + 1;
                    unsigned int p7 = (i + 1)*h*d + (j + 1)*d + k;
                    unsigned int p6 = p7 + 1;
                    unsigned int p4 = i*h*d + (j + 1)*d + k;
                    unsigned int p5 = p4 + 1;

                    // Ensure that neighboring tetras are sharing faces
                    if ((i + j + k) % 2 == 1)
                    {
                        f.push_back(Vec4i(p2,p1,p6,p3));
                        f.push_back(Vec4i(p6,p3,p4,p7));
                        f.push_back(Vec4i(p4,p1,p6,p5));
                        f.push_back(Vec4i(p3,p1,p4,p0));
                        f.push_back(Vec4i(p6,p1,p4,p3));
                    }
                    else
                    {
                        f.push_back(Vec4i(p0,p2,p5,p1));
                        f.push_back(Vec4i(p7,p2,p0,p3));
                        f.push_back(Vec4i(p5,p2,p7,p6));
                        f.push_back(Vec4i(p7,p0,p5,p4));
                        f.push_back(Vec4i(p0,p2,p7,p5));
                    }
                }
            }
        }
        nF = f.size();
    }
};

}

#endif // MESHLOADER_H
