#ifndef PBDMODEL_H
#define PBDMODEL_H

#include <bits/stdc++.h>
#include <Eigen/Dense>

#include "pbdConstraint.h"
#include "pbdState.h"
#include "mesh3d.h"
#include "mapping.h"

namespace pbd
{
using Vec3d = Eigen::Vector3d;
///
/// \brief The PositionBasedModel class
///
class PositionBasedModel
{
protected:

    Mesh*                                m_mesh;    
    MechanicalState*                     m_state;
    std::vector<Constraint*>             m_constraints;
    double mu, lambda; // Lame's constants

public:
    PositionBasedModel()
    {

    }

    inline void setModelGeometry(Mesh* m)
    {
        m_mesh = m;
        m_state = new MechanicalState;
        m_state->initialize(m_mesh);
    }    

    inline Mesh* getGeometry()
    {
        return m_mesh;
    }    

    inline MechanicalState* getState()
    {
        return m_state;
    }
    ///
    /// \brief setElasticModulus
    /// \param E  Young's modulus
    /// \param nu Poisson's ratio
    ///
    inline void setElasticModulus(const double& E, const double nu)
    {
        mu = E/(2*(1+nu));
        lambda = E*nu/((1-2*nu)*(1+nu));
    }    

    inline const double& getFirstLame() const
    {
        return mu;
    }

    inline const double& getSecondLame() const
    {
        return lambda;
    }


    ///
    /// \brief create constraints from the underlying mesh structure
    /// \param type could be Distance,Dihedral, Area, Volume, FEMTet, FEMHex, EdgeEdge, PointTriangle
    ///
    bool initConstraints(Constraint::Type type);
    ///
    /// \brief addConstraint add elastic constraint
    /// \param constraint
    ///
    inline void addConstraint(Constraint* constraint)
    {
        m_constraints.push_back(constraint);
    }

    ///
    /// \brief compute delta x and update position
    ///
    void constraintProjection(const unsigned int& maxIter);    

};

}

#endif // PBDMODEL_H
