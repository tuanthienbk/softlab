#ifndef MAPPING_H
#define MAPPING_H

#include <map>

#include "mesh3d.h"


namespace pbd {

class PositionBasedModel;
class CollisionModel;

class IMapping
{
public:
    IMapping() {}
};

class IdentityMapping : public IMapping
{
private:
    std::map<int,int> vmap;
public:
    IdentityMapping() {}
    void computeMap(TetMesh* vmesh, TriangleMesh* tmesh);
    void applyMapping(PositionBasedModel* model, CollisionModel* cmodel);
    void applyMapping(CollisionModel* cmodel, PositionBasedModel* model);

};

}
#endif // MAPPING_H
