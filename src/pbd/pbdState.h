#ifndef PBDSTATE_H
#define PBDSTATE_H

#include "mesh3d.h"

#include <Eigen/Dense>
#include <bits/stdc++.h>

namespace pbd
{

using Vec3d = Eigen::Vector3d;

class MechanicalState
{
private:

    std::vector<Vec3d> m_pos;
    std::vector<Vec3d> m_initPos;
    std::vector<Vec3d> m_vel;
    std::vector<Vec3d> m_acc;
    std::vector<Vec3d> m_oldPos;
    std::vector<double> m_mass;
    std::vector<double> m_invMass;

public:
    MechanicalState() = default;

    void initialize(Mesh* m)
    {
        m_pos = m->getVerticesPositions(); // share the same data with Mesh
        const int nP =  m->getNumVertices();
        m_initPos.assign(m_pos.begin(), m_pos.end());
        m_vel.resize(nP, Vec3d(0,0,0));
        m_acc.resize(nP, Vec3d(0,0,0));
        m_oldPos.resize(nP, Vec3d(0,0,0));
        m_invMass.resize(nP, 0);
        m_mass.resize(nP, 0);
    }

    inline void setUniformMass(const double& val)
    {
        if (val != 0.0) {
            std::fill(m_mass.begin(), m_mass.end(), val);
            std::fill(m_invMass.begin(), m_invMass.end(), 1/val);
        }
        else {
            std::fill(m_invMass.begin(), m_invMass.end(), 0.0);
            std::fill(m_mass.begin(), m_mass.end(), 0.0);
        }
    }

    inline void setParticleMass(const double& val, const unsigned int& idx)
    {
        if ( idx < m_pos.size()) {
            m_mass[idx] = val;
            m_invMass[idx] = 1/val;
        }
    }

    inline void setFixedPoint(const unsigned int& idx)
    {
        if ( idx < m_pos.size())
            m_invMass[idx] = 0;
    }

    inline double getInvMass(const unsigned int& idx)
    {
        return m_invMass[idx];
    }

    inline Vec3d& getInitialVertexPosition(const unsigned int& idx)
    {
        return m_initPos.at(idx);
    }

    inline Vec3d& getVertexPosition(const unsigned int& idx)
    {
        return m_pos.at(idx);
    }

    void integratePosition(const double& dt, const Vec3d& gravity)
    {
        for (int i = 0; i < m_pos.size(); ++i) {
           if (m_invMass[i] > 0) {
               m_vel[i] += (m_acc[i] + gravity)*dt;
               m_oldPos[i] = m_pos[i];
               m_pos[i] += m_vel[i]*dt;
           }
        }
    }

    void integrateVelocity(const double& dt)
    {
        for (int i = 0; i < m_pos.size(); ++i) {
           if (m_invMass[i] > 0) {
               m_vel[i] = (m_pos[i] - m_oldPos[i])/dt;
           }
        }
    }

    void computeAABB(double& min_x, double& max_x, double& min_y, double& max_y, double& min_z, double& max_z);
};

}

#endif // PBDSTATE_H
