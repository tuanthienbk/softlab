#ifndef PBDCOLLISIONMODEL_H
#define PBDCOLLISIONMODEL_H

#include "mesh3d.h"
#include "pbdState.h"

namespace pbd {

class CollisionModel
{
protected:
    double m_proximity;
    double m_contactStiffness;
    MechanicalState*    m_state;
    TriangleMesh*       m_mesh;
public:
    CollisionModel() {}

    inline void setModelGeometry(TriangleMesh* m)
    {
        m_mesh = m;
        m_state = new MechanicalState;
        m_state->initialize(m_mesh);
    }

    inline TriangleMesh* getGeometry()
    {
        return m_mesh;
    }

    inline MechanicalState* getState()
    {
        return m_state;
    }

    inline void setProximity(const double& prox)
    {
        m_proximity = prox;
    }

    inline double getProximity()
    {
        return m_proximity;
    }

    inline void setContactStiffness(const double& stiffness)
    {
        m_contactStiffness = stiffness;
    }

    inline double getContactStiffness()
    {
        return m_contactStiffness;
    }
};

}

#endif // PBDCOLLISIONMODEL_H
