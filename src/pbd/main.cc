#include "pbdModel.h"
#include "collision.h"

#include <vdb.h>

using namespace pbd;

void testVolume() {

    TetMesh* mesh = new TetMesh("cube_tet.veg");
    //    TetMesh* mesh = new TetMesh;
    //    mesh->asBeam(30, 5, 5);

    PositionBasedModel *model = new PositionBasedModel;
    model->setModelGeometry(mesh);
    model->setElasticModulus(1.0, 0.3);
    Vec3d g(0.0,-9.8,0.0);

    auto state = model->getState();
    state->setUniformMass(1.0);
    //    int fixedPoints[] = {51,52,103,104,155,156,207,208};
    int fixedPoints[] = {1};
    for (int i = 0; i < sizeof(fixedPoints)/sizeof(int); ++i) {
        state->setFixedPoint(fixedPoints[i]-1);
    }
    //    for (unsigned int j = 0; j < 5; j++)
    //    {
    //        for (unsigned int k = 0; k < 5; k++)
    //            state->setFixedPoint(j*5 + k);
    //    }

    model->initConstraints(Constraint::Type::FEMTet);
    //    model->initConstraints(Constraint::Type::Distance);
    //    model->initConstraints(Constraint::Type::Volume);

    double dt = 1e-3;

    while (1) {
        state->integratePosition(dt,g);
        model->constraintProjection(20);
        state->integrateVelocity(dt);
        vdb_begin();
        vdb_frame();
        vdb_color(0, 0, 1);
        for (int k = 0; k < mesh->getNumVertices(); ++k) {
            Vec3d& p = state->getVertexPosition(k);
            vdb_point(p[0], p[1], p[2]);
        }
        vdb_end();
    }
}

void testCloth() {
    TriangleMesh *mesh = new TriangleMesh;
    mesh->asSquareCloth(10.0, 10.0, 20, 20);
    PositionBasedModel *model = new PositionBasedModel;
    model->setModelGeometry(mesh);
    model->setElasticModulus(1.0, 0.3);
    Vec3d g(0.0,-9.8,0.0);

    auto state = model->getState();
    state->setUniformMass(1.0);
    int fixedPoints[] = {1,20};
    for (int i = 0; i < sizeof(fixedPoints)/sizeof(int); ++i) {
        state->setFixedPoint(fixedPoints[i]-1);
    }

    model->initConstraints(Constraint::Type::Distance);
    //    model->initConstraints(Constraint::Type::Area);
    model->initConstraints(Constraint::Type::Dihedral);

    double dt = 1e-3;

    while (1) {
        state->integratePosition(dt,g);
        model->constraintProjection(5);
        state->integrateVelocity(dt);
        vdb_begin();
        vdb_frame();

        for (int k = 0; k < mesh->getNumVertices(); ++k) {
            if (k == 0 || k == 19) {
                vdb_color(1, 0, 0);
            } else {
                vdb_color(0, 0, 1);
            }
            Vec3d& p = state->getVertexPosition(k);
            vdb_point(p[0], p[1], p[2]);
        }

        vdb_end();
    }
}

void testCollision()
{
    Vec3d g(0.0,-9.8,0.0);

    TetMesh* vmesh = new TetMesh;
    vmesh->asBeam(10, 5, 5);
    Vec3d scale(3,3,3);
    vmesh->scale(scale);

    PositionBasedModel *model1 = new PositionBasedModel;
    model1->setModelGeometry(vmesh);
    model1->setElasticModulus(1e-1, 0.3);
    auto state1 = model1->getState();
    state1->setUniformMass(1.0);
    model1->initConstraints(Constraint::Type::FEMTet);

    IdentityMapping* collisionMap1 = new IdentityMapping;
    TriangleMesh *tmesh = new TriangleMesh;
    collisionMap1->computeMap(vmesh, tmesh);
    CollisionModel *cm1 = new CollisionModel;
    cm1->setModelGeometry(tmesh);
    cm1->setProximity(0.1);
    cm1->setContactStiffness(1e-2);
    auto cs1 = cm1->getState();
    cs1->setUniformMass(1.0);

    Vec3d trans(1,5,1);
    vmesh->translate(trans);

    PositionBasedModel *model2 = new PositionBasedModel;
    model2->setModelGeometry(vmesh);
    model2->setElasticModulus(1e-1, 0.3);
    auto state2 = model2->getState();
    state2->setUniformMass(1.0);
    model2->initConstraints(Constraint::Type::FEMTet);

    IdentityMapping* collisionMap2 = new IdentityMapping;
    TriangleMesh *tmesh2 = new TriangleMesh;
    collisionMap2->computeMap(vmesh, tmesh2);
    CollisionModel *cm2 = new CollisionModel;
    cm2->setModelGeometry(tmesh2);
    cm2->setProximity(0.1);
    cm2->setContactStiffness(1e-2);
    auto cs2 = cm2->getState();
    cs2->setUniformMass(1.0);

    TriangleMesh *floor = new TriangleMesh;
    floor->asSquareCloth(100.0, 100.0, 2, 2);
    trans << -50, -3,-50;
    floor->translate(trans);
    floor->flipDirection();

    CollisionModel *floor_cm = new CollisionModel;
    floor_cm->setModelGeometry(floor);
    floor_cm->setProximity(0.1);
    floor_cm->setContactStiffness(1.0);
    auto floor_state = floor_cm->getState();
    floor_state->setUniformMass(0.0);

    std::vector< std::pair<CollisionModel*,CollisionModel*> > collisionPair;
    collisionPair.push_back(std::make_pair(cm1, floor_cm));
    collisionPair.push_back(std::make_pair(cm2, floor_cm));
    collisionPair.push_back(std::make_pair(cm1, cm2));

    Collision* collisionManager = new Collision;
    double dt = 1e-3;

    // animation loop
    while (1) {
        state1->integratePosition(dt,g);
        model1->constraintProjection(5);
        collisionMap1->applyMapping(model1,cm1);

        state2->integratePosition(dt,g);
        model2->constraintProjection(5);
        collisionMap2->applyMapping(model2,cm2);

        collisionManager->resetConstraints();
        for (int i = 0; i < collisionPair.size(); ++i) {
           if (collisionManager->doBroadPhase(collisionPair[i].first, collisionPair[i].second))
           {
               collisionManager->doNarrowPhase(collisionPair[i].first, collisionPair[i].second);
           }
        }
        collisionManager->doCollision(5);

        collisionMap1->applyMapping(cm1, model1);
        state1->integrateVelocity(dt);

        collisionMap2->applyMapping(cm2, model2);
        state2->integrateVelocity(dt);

        vdb_begin();
        vdb_frame();
//        vdb_color(1, 0, 0);
//        for (int k = 0; k < tmesh->getNumElements(); ++k) {
//            Vec3i& e = tmesh->getElement(k);
//            Vec3d& p0 = cs1->getVertexPosition(e[0]);
//            Vec3d& p1 = cs1->getVertexPosition(e[1]);
//            Vec3d& p2 = cs1->getVertexPosition(e[2]);
//            vdb_triangle(p0[0], p0[1], p0[2],
//                    p1[0], p1[1], p1[2],
//                    p2[0], p2[1], p2[2]);
//        }
//        vdb_color(0, 1, 0);
//        for (int k = 0; k < tmesh2->getNumElements(); ++k) {
//            Vec3i& e = tmesh2->getElement(k);
//            Vec3d& p0 = cs2->getVertexPosition(e[0]);
//            Vec3d& p1 = cs2->getVertexPosition(e[1]);
//            Vec3d& p2 = cs2->getVertexPosition(e[2]);
//            vdb_triangle(p0[0], p0[1], p0[2],
//                    p1[0], p1[1], p1[2],
//                    p2[0], p2[1], p2[2]);
//        }
        vdb_color(1, 0, 0);
        for (int k = 0; k < vmesh->getNumVertices(); ++k) {
           Vec3d& p = state1->getVertexPosition(k);
           vdb_point(p[0],p[1],p[2]);
        }
        vdb_color(0, 1, 0);
        for (int k = 0; k < vmesh->getNumVertices(); ++k) {
           Vec3d& p = state2->getVertexPosition(k);
           vdb_point(p[0],p[1],p[2]);
        }
        vdb_color(0, 0, 1);
        for (int k = 0; k < floor->getNumElements(); ++k) {
            Vec3i& e = floor->getElement(k);
            Vec3d& p0 = floor->getVerticesPosition(e[0]);
            Vec3d& p1 = floor->getVerticesPosition(e[1]);
            Vec3d& p2 = floor->getVerticesPosition(e[2]);
            vdb_triangle(p0[0], p0[1], p0[2],
                    p1[0], p1[1], p1[2],
                    p2[0], p2[1], p2[2]);
        }
        vdb_end();
    }

}

int main(int argc, char *argv[])
{

    //    testVolume();
        testCloth();
//    testCollision();
    return 0;
}
