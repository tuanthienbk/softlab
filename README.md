# README #

Experiment code for Material Point Method and Position Based Dynamics. 

[https://github.com/zdevito/vdb](https://github.com/zdevito/vdb) is required for visualization.

[OpenVDB](http://www.openvdb.org/) may be required in the future for MPM method in 3D, but not required now.

Demo: [https://youtu.be/ZoDyC4dt4K8](https://youtu.be/ZoDyC4dt4K8)